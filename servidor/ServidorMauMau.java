package servidor;

import javax.jws.WebService;

import ufg.Cliente;


/**
 * @author Lucas
 *
 */
@WebService(endpointInterface="servidor.IServidor")
public class ServidorMauMau implements IServidor {
	
	private int resp1 =0;
	private String nom1;
	private Jogador[] jogadores;
	// baralho da partida
	private Baralho baralho;
	// campeao da partida
	private Jogador campeao;
	// marcador fim de jogo
	private Boolean partidaFinalizada;
	// numero de jogadores que pararam e esperam por resultado
	private int jogadoresParados;

	private int contMonte = 0;

	private int mauMau = 0;

	private Cartas monte = null;

	private String finalJogo;
	// Quantidade de jogadores
	private int qtdJogadores;

	public ServidorMauMau(int qtdJogadores) {

		this.qtdJogadores = qtdJogadores;
		this.baralho = new Baralho(1, true);
		this.partidaFinalizada = false;
		this.jogadores = new Jogador[getQtdJogadores()];
		this.finalJogo = "";
	}

	

	@Override
	public String conecta(String nome){
		
		try {

			if(jogadores[jogadores.length - 1] == null){

				Jogador jogador = new Jogador();
				jogador.setNome(nome);
				
				for(int i = 0; i < jogadores.length; i++){
					if(jogadores[i] == null){
						jogadores[i] = jogador;
						i = jogadores.length;
					}
				}
	
				enviarMsg(jogador, "Voce esta conectado " + jogador.getNome() + "!");
				
				if(jogadores[jogadores.length - 1] == null)
					enviarMsg(jogador, "Aguardando conexoes....");
				
				else{
					new Thread(new ServidorThread(this)).start();
				}
			}
			
			else{
				return "Conexao recusada: já existem jogadores suficientes, espera a proxima";
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "ok";
		
	}


	// metodo que distribui cartas para os jogadores e verifica o ganhador
	public void distribuiCartas() throws InterruptedException {

		// contador de cartas/rodadas
		int k = 0;

		while (!partidaFinalizada && k <= 51) {

			for (int i = 0; i < this.jogadores.length; i++) {

				// a primeira rodada, apenas distribui cinco cartas a cada
				// jogador
				if (k < jogadores.length) {
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());
					this.jogadores[i].addCartas(baralho.darProximaCarta());

					k++;

				}

				// Se todos decidiram parar para receber o resultado no final do
				// jogo, então o campeao e informado
				else if (this.jogadoresParados == jogadores.length) {

					campeao = new Jogador();
					campeao.setSomaCartas(0);

					for (Jogador j : jogadores) {
						campeao = campeao.getSomaCartas() <= j.getSomaCartas() ? j : campeao;
					}

					this.partidaFinalizada = true;
					i = this.jogadores.length;
					informaCampeao();

				}

				else {
					int count = 0;// a partir da segunda rodada, conferir se
									// algum jogador acabou com todas as cartas
					for (int j = 0; j < jogadores.length; j++) {

						for (Cartas carta : jogadores[j].getMinhasCartas()) {

							if (carta != null) {

								count++;
							}
						}

						if (count == 0) {
							campeao = jogadores[j];
							partidaFinalizada = true;

						}
						j = jogadores.length;
					}
				}

			
				if(!partidaFinalizada && !jogadores[i].getParouDeJogar()){

					menu(jogadores[i]);
					
					while(!jogadores[i].getRespondeu())
						Thread.sleep(1000);
					
					enviarMsg(jogadores[i], "\n\nAguarde enquanto os outros jogadores fazem sua jogada");
				}
				//Mostra o vencedor do jogo
				else if(partidaFinalizada){
					i = this.jogadores.length;
					informaCampeao();
				}
			}
		}
	}


	
	/**
	 * Metodo que envia mensagens informando o vencedor do jogo.
	 */
	public void informaCampeao() {
		
		this.setFinalJogo("O jogador(a) " + campeao.getNome() + " ganhou o jogo!!! \n");

	}

	public void fecharConexaoJogadores(){

		for(Jogador j : jogadores)
			j = null;

	}

	public void menu(Jogador jogador) throws InterruptedException {

		boolean loop = true;
		if (contMonte == 0) {
			monte = baralho.darProximaCarta();
		} else {

		}
		contMonte++;
		String opcoes = "Digite uma das opcoes!!\r\n\r\n" + "1-Pegar Carta\r\n"
				+ "2-Desistir, voce deve aguardar ate o jogo terminar ou todos desistirem\r\n" + "3-Descartar\r\n"
				+ "4-Listar minhas cartas\r\n\r\n" + "5 - MAU MAU\r\n\r\n"
				+ "A carta do monte e: " + monte + "\r\n\r\n"
				+ "Digite o numero da opcao escolhida: \r\n\r\n";
	
		while (loop) {

		//	if (contMonte == 0) {
		//		monte = baralho.darProximaCarta();
		//	} else {

		//	}
			//contMonte++;
			 //System.out.println("SE RESPONDEU E  : "+jogador.getRespondeu());
			
			// enviarMsg(jogador, "A carta do monte e: " + monte);

			 enviarMsg(jogador, opcoes);
			 
			 jogador.setRespondeu(false);
			 while (!jogador.getRespondeu()){
				//this.getMenuResp(jogador);
				Thread.sleep(1000);
			}
	
			 
			 System.out.println("O GETMENURESP ANTERIOR E : "+ this.getMenuResp(jogador));
			 
			 if (this.getMenuResp(jogador) != 0) {
				// int respo = this.getMenuResp();
				 System.out.println("O GETMENURESP E : "+ this.getMenuResp(jogador));
				 switch (this.getMenuResp(jogador)) {
					//switch (Integer.parseInt(this.getStatus(jogador.getNome()))) {
					case 1:
						this.addCartas(jogador);
						loop = false;
						jogador.setStatus("Aguarde");
						jogador.setRespondeu(true);
						this.setMenuResp(0);
						break;
					case 2:
						jogador.setParouDeJogar(true);
						this.jogadoresParados++;
						loop = false;
						jogador.setStatus("Aguarde");
						jogador.setRespondeu(true);
						break;
					case 3:
						//mauMau = 0;
						loop = false;
						jogador.setStatus("Aguarde");
						jogador.setRespondeu(true);
						break;
					case 4:
						jogador.setStatus(listarCartas(jogador)+ "\n\n");
						enviarMsg(jogador, listarCartas(jogador) + "\n\n");
						jogador.setStatus("Aguarde");
						jogador.setRespondeu(false);
						break;
					case 5:
						//mauMau = 1;
						//verificaMauMau(jogador, mauMau);

						break;

					//default:
					//	enviarMsg(jogador, "Opcao invalida, digite uma correta" + "\n\n");
					//	break;
					}
			  
			 }}
	}

	public void addCartas(Jogador jogador) {

		jogador.addCartas(baralho.darProximaCarta());
		enviarMsg(jogador, listarCartas(jogador));

	}

	public Cartas rmCartas(Jogador jogador, int pos) {

		int count = 0;
		Cartas monte = null;
		for (Cartas carta : jogador.getMinhasCartas()) {
			count++;
			if (count == pos) {
				monte = jogador.rmCartas(pos);
			}

		}
		enviarMsg(jogador, listarCartas(jogador));

		return monte;
	}

	public boolean verificaJogada(Jogador jogador, int pos, Cartas monte) {
		boolean resp = false;
		int count = 0;
		for (Cartas carta : jogador.getMinhasCartas()) {
			count++;
			if (count == pos) {
				resp = jogador.verificaJogada(pos, monte);
			}

		}

		return resp;
	}

	public String verificaMauMau(String nome, int Mau) {
		// VERIFICA SE JOGADOR PEDIU MAU MAU OU ESQUECEU DE PEDIR
		int count = 0;
		String msg = "";
		for (Cartas carta : this.getJogadorByName(nome).getMinhasCartas()) {

			if (carta != null) {
				count++;
			}
		}
		if ((Mau == 0) && (count == 1)) { // Esqueceu de pedir
			String msg1 = ("Voce esqueceu de pedir MAU MAU, receba 5 cartas.");
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			Mau = 0;
			msg = msg1;
		} else if ((Mau == 1) && (count != 1)) { // Pediu errado
			String msg2 = ("Pediu MAU MAU na hora errada!, receba 5 cartas.");
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			this.getJogadorByName(nome).addCartas(baralho.darProximaCarta());
			Mau = 1;
			msg = msg2;
		} else {
			// Pediu certo ou n�o pediu mau mau na hora certa
		}
		return msg;

	}

	public String listarCartas(Jogador jogador) {
		String mensagem = "\nLista de cartas\n";
		int count = 0;
		for (Cartas carta : jogador.getMinhasCartas()) {
			count++;
			if (carta != null)

				mensagem += String.valueOf(count) + " - " + carta.toString() + "\n";

		}

		return mensagem;

	} 

	public Integer lerMsgMenu(Jogador jogador, String msg){
		 
		//int resp = Integer.parseInt(this.getStatus(jogador.getNome()));
		//System.out.println(resp + " AQUE OW");
		try {
			return getMenuResp(jogador); //resp;//j ogador.getStub().getRespostaMenu(msg);
			
		} catch (Exception e) {
			enviarMsg(jogador, "Deu pau.");
			return null;
			
		}
	}

	public Boolean enviarMsg(Jogador jogador, String msg){

		
		
		String msgEnvio = "\n";
		msgEnvio += msg;

		jogador.setStatus(msgEnvio);

		System.out.println(msgEnvio);

		return true;
	}

	/**
	 * Metodo que envia a mensagem para todos os jogadores
	 */
	public Boolean enviarMsg(String msg){

		String msgEnvio = "\nMensagem para todos os jogadores: ";
		msgEnvio += msg;

			for(Jogador jogador : jogadores){
			
				if(jogador != null){
					jogador.setStatus(msgEnvio);
				}
			}
			
		System.out.println(msgEnvio);

		return true;
	}

	public void apagaStatus(Jogador jogador){
		
		if(!getStatus(jogador.getNome()).startsWith("Digite"))
			setStatus(jogador.getNome(), "");
		
	}

	public int getJogadoresParados() {
		return jogadoresParados;
	}

	public void setJogadoresParados(int jogadoresParados) {
		this.jogadoresParados = jogadoresParados;
	}

	public int getQtdJogadores() {
		return qtdJogadores;
	}

	public void setQtdJogadores(int qtdJogadores) {
		this.qtdJogadores = qtdJogadores;
	}
	
	@Override
	public String getStatus(String nome) {
		// TODO Auto-generated method stub
		return this.getJogadorByName(nome).getStatus();
	}
	
	public void setStatus(String nome, String status){
		this.getJogadorByName(nome).setStatus(status);
		
	}
	public void setJogadorRespondeu(String nome){
		this.getJogadorByName(nome).setRespondeu(true);
	}
	@Override
	public Boolean getPartidaFinalizada() {
		return partidaFinalizada;
	}

	
	public Jogador getJogadorByName(String nome) {
		
		for(Jogador j : this.jogadores){
			if(j.getNome().equals(nome))
				return j;
		}
		return null;
	}

	public String getFinalJogo() {
		return finalJogo;
	}

	public void setFinalJogo(String finalJogo) {
		this.finalJogo = finalJogo;
	}



	@Override
	public String listarCartas(String nome) {
		String mensagem = "\nLista de cartas\n";
		int count = 0;
		for (Cartas carta : this.getJogadorByName(nome).getMinhasCartas()) {
			count++;
			if (carta != null)

				mensagem += String.valueOf(count) + " - " + carta.toString() + "\n";

		}

		return mensagem;

	
	}



	@Override
	public Integer getSomaCartas(String nome) {
		// TODO Auto-generated method stub
		return null;
	}



	@Override
	public void menuResp(Integer resp1, String nom) {
		// TODO Auto-generated method stub
		this.resp1 = resp1;
		System.out.println("DIGITARAM AQUI Ó: "+ resp1);
	}
	public void setMenuResp(Integer resp1) {
		// TODO Auto-generated method stub
		this.resp1 = resp1;
	}
	public Integer getMenuResp(Jogador jogador){
		return resp1;
	}
	
	public String validaDescarte(String nome, int op){
		String mensagem = "\nJogada Efetuada Com Sucesso!\n";
		if (this.getJogadorByName(nome).verificaJogada(op, monte) == true) {
			monte = rmCartas(this.getJogadorByName(nome), op);
			this.getJogadorByName(nome).setStatus("Aguarde");
			this.getJogadorByName(nome).setRespondeu(true);
		} else {
			return "\nVoce nao pode fazer esta jogada, atente-se para as regras\n";
		}
		return mensagem;
		}
	}


	
