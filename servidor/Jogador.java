package servidor;
/**
 * @author lucas
 *
 */

import java.io.*;
import java.net.*;
import java.util.Scanner;

import javax.swing.JOptionPane;

public class Jogador{
	private String nome;
	//indica a porta usada
	private int porta;
	//Endereco IP do servidor, fixo
	private InetAddress ipServidor;
	//Endereco IP do Jogador
	private Boolean respondeu = false;
	// as cartas do jogador no jogo
	private Cartas[] minhasCartas;
	// soma das cartas
	private int somaCartas;
	//indica se o jogador parou de jogar e está esperando o resultado final
	private boolean parouDeJogar;
	//Indica o status do jogador
	private String status;
	private int resposta;
		
	public Jogador(){
		this.minhasCartas = new Cartas[26];
		this.somaCartas = 0;
		this.status = "Aguarde";
		this.respondeu = false;
	}

	
	public Jogador(InetAddress ipServidor, int porta, InetAddress ip) {
		this.ipServidor = ipServidor;
		this.setPorta(porta);
		//considerando o baralho com 52 cartas, e o jogo com no minimo 2 jogadores, 
		//entao o maximo de cartas de um jogador é 26
		this.minhasCartas = new Cartas[26];
		this.somaCartas = 0;
	}


	public int getPorta() {
		return porta;
	}


	public void setPorta(int porta) {
		this.porta = porta;
	}


	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Cartas[] getMinhasCartas() {
		return minhasCartas;
	}
	
	public Cartas rmCartas(int pos) {
		Cartas cartaMonte = minhasCartas[pos-1];
		minhasCartas[pos-1]=null;
		
		return cartaMonte;
	}
	public boolean verificaJogada(int pos, Cartas cartaMonte){ 
		if (( cartaMonte.getNumeroCarta() == minhasCartas[pos-1].getNumeroCarta()) || ( cartaMonte.getNaipesCarta().equals( minhasCartas[pos-1].getNaipesCarta()) ) ) {
			return true;
		}else
		{
			return false;
		}
	}
	
	
	public void addCartas(Cartas carta) {
		
		int i = 0;
		
		for(i = 0; i < minhasCartas.length; i++){
			
			if(minhasCartas[i] == null){
				
				minhasCartas[i] = carta;
				i = minhasCartas.length;
			}	
		}
	}

	
	public Integer getRespostaMenu(String msg){
		Scanner scanner = new Scanner(System.in);
		boolean respCorreta = true;
		while(respCorreta){
			try {
				System.out.println(msg);
				resposta = scanner.nextInt();
				respCorreta = false;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Resposta inválida! Digite somente números");
			}
		}
		return resposta;
	}

	public int getSomaCartas() {
		return somaCartas;
	}

	public void setSomaCartas(int somaCartas) {
		this.somaCartas = somaCartas;
	}

	public boolean getParouDeJogar() {
		return parouDeJogar;
	}

	public void setParouDeJogar(boolean parouDeJogar) {
		this.parouDeJogar = parouDeJogar;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getRespondeu() {
		return respondeu;
	}
	public void setRespondeu(Boolean respondeu) {
		this.respondeu = respondeu;
	}
}