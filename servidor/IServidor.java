package servidor;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;

@WebService
@SOAPBinding(style = Style.RPC)

public interface IServidor{

	@WebMethod String conecta(String nome);
	@WebMethod String getStatus(String nome);
	@WebMethod void setStatus(String nome, String status);
	@WebMethod void setJogadorRespondeu(String nome);
	@WebMethod Boolean getPartidaFinalizada();
	@WebMethod void menuResp(Integer resp, String nome);
	@WebMethod String listarCartas(String nome);
	@WebMethod Integer getSomaCartas(String nome);
	@WebMethod String getFinalJogo();
	@WebMethod String validaDescarte(String nome, int op);
	@WebMethod String verificaMauMau(String nome, int Mau);
	//void menuResp(Integer resp1, String nom, Jogador jogador);
	
}
