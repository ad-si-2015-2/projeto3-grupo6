package servidor;

public class ServidorThread implements Runnable {

	private ServidorMauMau servidor;
	
	public ServidorThread(ServidorMauMau servidor) {
		this.servidor = servidor;
	}
	
	@Override
	public void run() {
		
		try {
			Thread.sleep(500);
			//Comunica a todos que o jogo vai comecar!
			servidor.enviarMsg("O jogo vai comecar em...");

			for(Integer i = 3; i > 0; i--){						
				servidor.enviarMsg(i.toString());
				Thread.sleep(1000);
			}
			
			servidor.distribuiCartas();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
