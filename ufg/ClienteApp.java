package ufg;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import servidor.IServidor;

public class ClienteApp {

	public static void main(String[] args) {

		Cliente cliente = new Cliente();
		cliente.setNome(cliente.getResposta("Digite o seu nome: "));
		int var = 0;

		try {
			URL url = new URL("http://localhost:1200/servidor?wsdl");
			QName qname = new QName("http://servidor/", "ServidorMauMauService");
			Service ws = Service.create(url, qname);
			IServidor servidor = ws.getPort(IServidor.class);
			
			if(!servidor.conecta(cliente.getNome()).equals("ok"))
				cliente.terminarExecucao();
			
			String status = null;
			int statusCount = 0;
			String statusAux = null;
			
			while(!servidor.getPartidaFinalizada()){
				
				status = servidor.getStatus(cliente.getNome());
				
				statusAux = statusCount == 0 ? servidor.getStatus(cliente.getNome()) : statusAux;
				
				if(!status.startsWith("Aguard") && !status.startsWith("\nAguard")){
					
					//menu
					if(status.startsWith("Digite") || status.startsWith("\nDigite")){	
						int resp = cliente.getRespostaMenu(status);
						
						
						if(resp == 3){
							cliente.imprime(servidor.verificaMauMau(cliente.getNome(), var));
							var = 0;
							cliente.imprime(servidor.listarCartas(cliente.getNome()) + "\n\n");
							int opcao = cliente.getRespostaMenu("Escolha a opcao correspondente a carta que sera descartada: " + "\n\n");	
							String txt = servidor.validaDescarte(cliente.getNome(),opcao);
							cliente.imprime(txt);
							if (txt.startsWith("\nJogada")){
								
								servidor.menuResp(3, cliente.getNome());
								servidor.setStatus(cliente.getNome(), "");
								servidor.setJogadorRespondeu(cliente.getNome());
							}
							
							
						}
						else if(resp == 4){
							cliente.imprime(servidor.listarCartas(cliente.getNome()) + "\n\n");
							servidor.menuResp(resp, cliente.getNome());
							servidor.setStatus(cliente.getNome(), "");
							servidor.setJogadorRespondeu(cliente.getNome());
						}else if (resp == 5 ){
							var = 1;
							String text = servidor.verificaMauMau(cliente.getNome(), var);
							
							cliente.imprime(text);
						
						}
						else if(resp > 5){
							cliente.imprime("Opcao invalida, digite uma correta" + "\n\n");
						//var = 1; variavel que seta se  o user digitou maumau 
						}
						else{
							servidor.menuResp(resp, cliente.getNome());
							servidor.setStatus(cliente.getNome(), "");
							servidor.setJogadorRespondeu(cliente.getNome());
						}
					}
					
					
					else{
						if(statusCount == 0){
							cliente.imprime(status);
							statusCount++;
						}
						else if(!status.equals(statusAux)){
							cliente.imprime(status);
							statusCount = 0;
						}						
					}
				}
				
				else{
					cliente.imprime(status);
					servidor.setStatus(cliente.getNome(), "");
					Thread.sleep(500);
				}
			}
			
			cliente.imprime(servidor.getFinalJogo());
			cliente.terminarExecucao();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
