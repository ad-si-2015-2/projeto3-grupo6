package ufg;


import java.util.Scanner;

import javax.swing.JOptionPane;

public class Cliente implements ICliente{

	private String nome;
	private Scanner scanner;
	private Integer numero;
	
	public Cliente(){
		scanner = new Scanner(System.in);
	}
	

	@Override
	public void imprime(String msg){
		if(msg != null && !msg.equals(""))
			System.out.println(msg);

	}

	@Override
	public Integer getRespostaMenu(String msg){

		Integer resposta = null;
		boolean respCorreta = true;

		while(respCorreta){
			try {
				System.out.println(msg);
				resposta = scanner.nextInt();
				respCorreta = false;
			} catch (Exception e) {
				JOptionPane.showMessageDialog(null, "Resposta inválida! Digite somente números");
			}
		}
		return resposta;
	}

	@Override
	public String getResposta(String msg){
		return JOptionPane.showInputDialog(msg);
	}
	
	public void terminarExecucao(){
		System.exit(0);
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}


	public Integer getNumero() {
		return numero;
	}


	public void setNumero(Integer numero) {
		this.numero = numero;
	}

}
